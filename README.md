# livtools: Estimation and Simulation of Animal Production

Livtools is a collection of quantitative tools for estimating and simulating animal production in extensive tropical farming systems.

## Description

### Livestock information system

Two survey methods for estimating ruminant population rates in tropical or Mediterranean environments where there is no systematic system for collecting herd demographics: animal monitoring with individual identification (**Laser**) and cross-sectional retrospective surveys (**12mo**). The information system consists in a set of standardized questionnaires, an Microsoft Access database and an interface for data entry and managing and R package for calculating demographic parameters. 

- [Laser](https://gitlab.cirad.fr/selmet/livtools/laser): Information system for studying livestock production and animal health using animal-based herd monitoring

- [12mo](https://gitlab.cirad.fr/selmet/livtools/12mo): Retrospective method for estimating annual demographic parameters in ruminants livestock herds

- [laserdemog](https://gitlab.cirad.fr/selmet/livtools/laserdemog): R package using a discrete-time approach for estimating demographic parameters from animal-based monitoring Laser data

- [t12mo](https://gitlab.cirad.fr/selmet/livtools/t12mo): R package for calculating demographic parameters of livestock populations from 12mo survey data

### Animal population dynamics

Animal population projection models tools proposed have been designed for simulating livestock populations in which animals are extensively managed. Models can be used to simulate livestock production over one or more years based on natural animal demographic parameters such as fecundity and mortality, and herd management parameters such as sales and purchases. 
**Dynmod** is a simplified and pedagogical version of age-structured population matrix models in the **mmage** R package.


- [mmage](https://gitlab.cirad.fr/selmet/livtools/mmage): R package for simulating age-structured population matrix models

- [Dynmod](https://gitlab.cirad.fr/selmet/livtools/dynmod): A spreadsheet interface for demographic projections of livestock populations

## Support

Please send a message to the maintainer: Julia Vuattoux <julia.vuattoux@cirad.fr>

## License

[GPL (\>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
